import cv2
import glob
from moviepy.editor import *


def change_fps(path_to_vid, fps):
    video = VideoFileClip(path_to_vid)
    video = video.set_fps(fps)
    video.write_videofile(f"./temp/fps/{str(fps)}.mp4")
    video.close()


def get_frames(capture, frame_output):
    frame_number = 0

    while True:
        success, frame = capture.read()
        if success:
            cv2.imwrite(f"{frame_output}/{frame_number}.jpg", frame)
        else:
            break

        frame_number += 1

    capture.release()


def stitch_frames(frame_folder, fps):
    image_array = []
    frame_number = 0
    for infile in glob.glob(f'{frame_folder}/*.jpg'):
        image = cv2.imread(f'{frame_folder}/{frame_number}.jpg')
        height, width, layers = image.shape
        size = (width, height)
        image_array.append(image)
        frame_number += 1

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    out = cv2.VideoWriter('dreamed.mp4', fourcc, fps, size)

    for i in range(len(image_array)):
        out.write(image_array[i])

    out.release()


def get_audio(path_to_vid, audio_output_path):
    video = VideoFileClip(path_to_vid)
    video.audio.write_audiofile(f"{audio_output_path}/audio.mp3")
    video.close()


def apply_audio(path_to_vid, path_to_audio):
    video = VideoFileClip(path_to_vid)
    audio = AudioFileClip(path_to_audio)

    new_audio = CompositeAudioClip([audio])
    video.audio = new_audio
    video.write_videofile("./dreamed_with_audio.mp4")
    video.close()
