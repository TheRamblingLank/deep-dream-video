from deepdreamer import load_image, recursive_optimize
import numpy as np
import PIL.Image


def dream_image(file_name, layer, iterations, repeats, model, session):
    img_result = load_image(filename='{}'.format(file_name))
    img_result = recursive_optimize(layer_tensor=layer, image=img_result,
                                    # how clear is the dream vs original image
                                    num_iterations=iterations, step_size=1.0, rescale_factor=0.5,
                                    # How many "passes" over the data. More passes, the more granular the gradients will
                                    # be.
                                    num_repeats=repeats, blend=0.2, model=model, session=session)
    img_result = np.clip(img_result, 0.0, 255.0)
    img_result = img_result.astype(np.uint8)
    result = PIL.Image.fromarray(img_result, mode='RGB')
    return result
