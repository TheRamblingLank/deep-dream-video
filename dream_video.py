"""
layer 1: wavy
layer 2: lines
layer 3: boxes (20 iterations is good)
layer 4: circles
layer 6: dogs, bears, cute animals.
layer 7: faces, buildings
layer 8: fish begin to appear, frogs/reptilian eyes.
layer 10: Monkeys, lizards, snakes, duck (needs 40 iterations)
"""

import video_handler
import dream_image
import os
import time
import inception5h
import tensorflow as tf
import cv2
import datetime

frames_output = "./temp/vid_out"
dreamed_frames_output = "./temp/vid_frames_dream_out"
adjusted_video_output = "./temp/fps"
audio_out = "./temp/audio"


def create_temp_directories():
    try:
        os.makedirs(f"{dreamed_frames_output}/")
    except FileExistsError:
        pass
    try:
        os.makedirs(f"{frames_output}/")
    except FileExistsError:
        pass
    try:
        os.makedirs(f"{adjusted_video_output}/")
    except FileExistsError:
        pass
    try:
        os.makedirs(f"{audio_out}/")
    except FileExistsError:
        pass


def clean_temp_directories():
    files = [f for f in os.listdir(dreamed_frames_output) if f.endswith(".jpg")]
    for f in files:
        os.remove(os.path.join(dreamed_frames_output, f))
    print("Cleared temp directory 1...")

    files = [f for f in os.listdir(frames_output) if f.endswith(".jpg")]
    for f in files:
        os.remove(os.path.join(frames_output, f))
    print("Cleared temp directory 2...")

    files = [f for f in os.listdir(adjusted_video_output) if f.endswith(".mp4")]
    for f in files:
        os.remove(os.path.join(adjusted_video_output, f))
    print("Cleared temp directory 3...")

    files = [f for f in os.listdir(audio_out) if f.endswith(".mp3")]
    for f in files:
        os.remove(os.path.join(audio_out, f))
    print("Cleared temp directory 4...")


def main():
    fps = 12

    create_temp_directories()
    clean_temp_directories()

    video_handler.get_audio("./video/CoL.mp4", audio_out)

    video_handler.change_fps("./video/CoL.mp4", fps=fps)

    video_path = f"./temp/fps/{fps}.mp4"

    video_capture = cv2.VideoCapture(video_path)

    video_handler.get_frames(video_capture, frames_output)

    num_images = len(os.listdir(frames_output))

    for i in range(0, num_images - 1):
        model = inception5h.Inception5h()
        experimental = tf.compat.v1.GPUOptions.Experimental(use_unified_memory=True)
        gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8, experimental=experimental)
        session = tf.compat.v1.Session(graph=model.graph,
                                       config=tf.compat.v1.ConfigProto(gpu_options=gpu_options))
        layer_tensor = model.layer_tensors[3]

        print(f"Processing image {i+1}/{num_images}")
        start = time.time()
        dream = dream_image.dream_image(f'{frames_output}/{i}.jpg', layer_tensor, 20, 8, model, session)
        dream.save(f'{dreamed_frames_output}/{i}.jpg')
        dream.close()
        secs = time.time() - start
        print(f"\nThis image took {secs}s")
        remaining_secs = secs * (num_images - (i + 2))
        conversion = datetime.timedelta(seconds=remaining_secs)
        print(f"Projected remaining time: {conversion}")

    video_handler.stitch_frames(dreamed_frames_output, fps)

    video_handler.apply_audio("./dreamed.mp4", f"{audio_out}/audio.mp3")

    #clean_temp_directories()


if __name__ == "__main__":
    main()
