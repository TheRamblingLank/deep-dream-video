# Welcome to a Deep Dream Video Repo
This project automates deep dreaming video. It is still a <b>WIP</b>.

## Contents
1. [Requirements](#req)
2. [Usage](#useage)

## Requirements <a name="req"></a>
The code should work fine with a standard tensorflow env, although you will need to install opencv-python and moviepy.

For better results, and to utilise your graphics card, install compatible CUDA and cuDNN versions and use a 
tensorflow-gpu env.

## Usage
Oops, there's nothing here yet...
